/*
 *Los DAO's es un componente de software que suministra una interfaz común entre la aplicación y uno o más 
 *dispositivos de almacenamiento de datos, tales como una Base de datos. En este caso estaremos creaando el 
 *DAO de Dispositivo.
 */
package com.example.alpha.repository;

//Librerías
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.alpha.entity.CuponAplicado;

@Repository //Es un marcador para la clase CuponAplicado que cumpla con el rol o el estereotipo del repositorio 
public interface CuponAplicadoRepository extends JpaRepository<CuponAplicado, UUID>
{

}
