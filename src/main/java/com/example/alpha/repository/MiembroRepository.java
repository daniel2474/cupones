/*
 *Los DAO's es un componente de software que suministra una interfaz común entre la aplicación y uno o más 
 *dispositivos de almacenamiento de datos, tales como una Base de datos. En este caso estaremos creaando el 
 *DAO de Miembro.
 */

package com.example.alpha.repository;

//Librerías
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.alpha.entity.Miembro;

@Repository //Es un marcador para la clase Miembro que cumpla el rol o el estereotipo del repositorio.
public interface MiembroRepository extends JpaRepository<Miembro, UUID> {

}
