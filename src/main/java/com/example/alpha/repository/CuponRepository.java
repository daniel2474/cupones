/*
 *Los DAO's es un componente de software que suministra una interfaz común entre la aplicación y uno o más 
 *dispositivos de almacenamiento de datos, tales como una Base de datos. En este caso estaremos creaando el 
 *DAO de Cupon.
 */
package com.example.alpha.repository;

//Librerías
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.alpha.entity.Cupon;

@Repository //Es un marcador para la clase Cupon que cumpla el rol o el estereotipo del repositorio.
public interface CuponRepository extends JpaRepository<Cupon, UUID> {

}
