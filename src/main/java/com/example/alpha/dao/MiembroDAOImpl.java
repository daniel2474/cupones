package com.example.alpha.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.alpha.entity.Miembro;



@Component("miembroDAOImpl")
@Repository 
public class MiembroDAOImpl implements MiembroDAO
{

	@Autowired
	private EntityManager entityManager;
	
	@Override 
	@Transactional 
	public List<Miembro> findAll() 
	{
	     Session currentSession = entityManager.unwrap(Session.class);
	     Query<Miembro> theQuery = currentSession.createQuery("from Miembro", Miembro.class);
	     List<Miembro> miembro = theQuery.getResultList();
	     return miembro;
	}
	
	@Override
	@Transactional
	public Miembro findById(int id) 
	{
		Session currentSession = entityManager.unwrap(Session.class);
		Miembro miembro = currentSession.get(Miembro.class, id);
        return miembro;
	}
	
	@Override
    @Transactional
	public void save(Miembro miembro) 
	{
		Session currentSession = entityManager.unwrap(Session.class);
        currentSession.saveOrUpdate(miembro);  
	}
}
