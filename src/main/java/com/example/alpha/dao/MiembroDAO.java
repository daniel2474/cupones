package com.example.alpha.dao;

import java.util.List;
import com.example.alpha.entity.Miembro;

public interface MiembroDAO 
{
	public List<Miembro> findAll();
    public Miembro findById(int id);
    public void save(Miembro miembro);	
}
