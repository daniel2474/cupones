/**
 * 	Esta clase permite hacer uso de todos los service para crear, actualizar y obtener las entidades mapeadas
 * @author: Daniel García Velasco y Abimael Rueda Galindo
 * @version: 12/7/2021
 *
 */
package com.example.alpha.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.alpha.entity.Cupon;
import com.example.alpha.entity.CuponAplicado;
import com.example.alpha.entity.Dispositivo;
import com.example.alpha.entity.Miembro;
import com.example.alpha.repository.CuponAplicadoRepository;
import com.example.alpha.repository.CuponRepository;
import com.example.alpha.repository.DispositivoRepository;
import com.example.alpha.repository.MiembroRepository;

@RestController
@RequestMapping("/alpha")
public class Servicios 
{
	//Service para Miembro
	@Autowired
	private MiembroRepository miembroRepository;

	//Service para Dispositivo
	@Autowired
	private DispositivoRepository dispositivoRepository;
	
	//Service para Cupón
	@Autowired
	private CuponRepository cuponRepository;
	
	//Service para CuponAplicado
	@Autowired 
	private CuponAplicadoRepository cuponAplicadoRepository;
	
	//----------------------------------------MIEMBRO------------------------------------------------
	/**
	 * Metodo que muestra todos los Miembros almacenados en la base de datos
	 * @return lista de Miembro
	 */
	@RequestMapping(value="obtenerMiembro", method=RequestMethod.GET)
	public ResponseEntity<List<Miembro>> obtenerCliente()
	{
		List<Miembro> miembros = miembroRepository.findAll();
		return ResponseEntity.ok(miembros);
	}
	
	/**
	 * Metodo que muestra solo un miembro
	 * @param Id es el id del miembro que se quiere mostrar, en caso de no encontrarlo genera un RuntimeException
	 * @return El miembro con el id 
	 */
	@GetMapping("/obtenerMiembro/{id}")
	@ResponseBody
    public Miembro getMiembro(@PathVariable UUID id){
		Optional<Miembro> OpcionalMiembro = miembroRepository.findById(id);
		if(OpcionalMiembro.isPresent())
		{
	        return OpcionalMiembro.get();
        }
		return null;
    }//fin del metodo
	
	/**
	 * Metodo que añade a la base de datos un nuevo miembro
	 * @param miembro es el objeto miembro que se desea añadir, en caso de contar con el mismo id, actualiza los valores solamente
	 * @return el objeto miembro que fue almacenado
	 */
	@RequestMapping(value="crearMiembro", method=RequestMethod.POST)
	public ResponseEntity<Miembro> crearCliente(@RequestBody Miembro miMiembro)
	{
		Miembro nuevomiembro = miembroRepository.save(miMiembro);
		nuevomiembro.setFechaModificacion(new Date());
		nuevomiembro.setFechaCreacion(new Date());
		nuevomiembro.setActivo(true);
		return ResponseEntity.ok(nuevomiembro);
	}
	
	/**
	 * Metodo que modifica un miembro ya existente en la base de datos (el miembro debe existir sino sera creado uno nuevo)
	 * @param miembro es el objecto miembro que se quiere modificar
	 * @return objeto miembro ya modificado
	 */
	@RequestMapping(value="actualizarMiembro", method=RequestMethod.PUT)
	public ResponseEntity<Miembro> actualizarCliente(@RequestBody Miembro miMiembro)
	{
		Optional<Miembro> OpcionalMiembro = miembroRepository.findById((UUID) miMiembro.getId());
		if(OpcionalMiembro.isPresent())
		{
			Miembro actualizarMiembro = OpcionalMiembro.get();
			actualizarMiembro.setNombre(miMiembro.getNombre());
			actualizarMiembro.setClub(miMiembro.getClub());
			actualizarMiembro.setTipoMembresia(miMiembro.getTipoMembresia());
			actualizarMiembro.setActivo(miMiembro.isActivo());
			actualizarMiembro.setFechaModificacion(new Date());
			miembroRepository.save(actualizarMiembro);
			return ResponseEntity.ok(actualizarMiembro);
		}
		else
		{
			return ResponseEntity.notFound().build();
		}
	}
	
	@RequestMapping(value="eliminarMiembro", method=RequestMethod.DELETE)
	public ResponseEntity<Void> eliminarMiembro(@PathVariable("productId") UUID Id)
	{
		miembroRepository.deleteById(Id);	
		return ResponseEntity.ok(null);
	}
	
	//----------------------------------------DISPOSITIVO------------------------------------------------
	/**
	 * Metodo que muestra todos los Dispositivos almacenados en la base de datos
	 * @return lista de dispositivos
	 */
		@RequestMapping(value="obtenerDispositivo", method=RequestMethod.GET)
		public ResponseEntity<List<Dispositivo>> obtenerDispositivo()
		{
			List<Dispositivo> dispositivos = dispositivoRepository.findAll();
			return ResponseEntity.ok(dispositivos);
		}
		
		/**
		 * Metodo que muestra solo un dispositivo
		 * @param Id es el id del dispositivo que se quiere mostrar, en caso de no encontrarlo genera un RuntimeException
		 * @return El dispositivo con el id Id
		 */
		@GetMapping("/obtenerDispositivo/{id}")
		@ResponseBody
	    public Dispositivo getDispositivo(@PathVariable UUID id){
			Optional<Dispositivo> OpcionalDispositivo = dispositivoRepository.findById(id);
			if(OpcionalDispositivo.isPresent())
			{
		        return OpcionalDispositivo.get();
	        }
			return null;
	    }//fin del metodo
		
		/**
		 * Metodo que añade a la base de datos un nuevo dispositivo
		 * @param dispositivo es el objeto dispositivo que se desea añadir, en caso de contar con el mismo id, actualiza los valores solamente
		 * @return el objeto dispositivo que fue almacenado
		 */
		@RequestMapping(value="crearDispositivo", method=RequestMethod.POST)
		public ResponseEntity<Dispositivo> crearDispositivo(@RequestBody Dispositivo miDispositivo)
		{
			Dispositivo nuevodispositivo = dispositivoRepository.save(miDispositivo);
			nuevodispositivo.setFechaModificacion(new Date());
			nuevodispositivo.setFechaCreacion(new Date());
			nuevodispositivo.setActivo(true);
			return ResponseEntity.ok(nuevodispositivo);
		}
		
		/**
		 * Metodo que modifica un dispositivo ya existente en la base de datos (el dispositivo debe existir sino sera creado uno nuevo)
		 * @param dispositivo es el objecto dispositivo que se quiere modificar
		 * @return objeto dispositivo ya modificado
		 */
		@RequestMapping(value="actualizarDispositivo", method=RequestMethod.PUT)
		public ResponseEntity<Dispositivo> actualizarDispostivo(@RequestBody Dispositivo miDispositivo)
		{
			Optional<Dispositivo> OpcionalDispositivo = dispositivoRepository.findById((UUID) miDispositivo.getId());
			if(OpcionalDispositivo.isPresent())
			{
				Dispositivo actualizarDispositivo = OpcionalDispositivo.get();
				actualizarDispositivo.setModelo(miDispositivo.getModelo());
				actualizarDispositivo.setClub(miDispositivo.getClub());
				actualizarDispositivo.setUbicacion(miDispositivo.getUbicacion());
				actualizarDispositivo.setSerie(miDispositivo.getSerie());
				actualizarDispositivo.setUltimaSincronicacion(miDispositivo.getUltimaSincronicacion());
				actualizarDispositivo.setActivo(miDispositivo.isActivo());
				actualizarDispositivo.setFechaModificacion(new Date());
				dispositivoRepository.save(actualizarDispositivo);
				return ResponseEntity.ok(actualizarDispositivo);
			}
			else
			{
				return ResponseEntity.notFound().build();
			}
		}	
		
		@RequestMapping(value="eliminarDispositivo", method=RequestMethod.DELETE)
		public ResponseEntity<Void> eliminarDispositivo(@PathVariable("dispositivoId") UUID Id)
		{
			dispositivoRepository.deleteById(Id);
			return ResponseEntity.ok(null);
		}
		
		//----------------------------------------CUPÓN-----------------------------------------------
		/**
		 * Metodo que muestra todos los Cupones almacenados en la base de datos
		 * @return lista de Cupones
		 */
		@RequestMapping(value="obtenerCupon", method=RequestMethod.GET)
		public ResponseEntity<List<Cupon>> obtenerCupon()
		{
			List<Cupon> cupones = cuponRepository.findAll();
			return ResponseEntity.ok(cupones);
		}
		
		/**
		 * Metodo que muestra solo un cupon
		 * @param Id es el id del cupon que se quiere mostrar, en caso de no encontrarlo genera un RuntimeException
		 * @return El cupon con el id Id
		 */
		@GetMapping("/obtenerCupon/{id}")
		@ResponseBody
	    public Cupon getCupon(@PathVariable UUID id){
			Optional<Cupon> OpcionalCupon = cuponRepository.findById(id);
			if(OpcionalCupon.isPresent())
			{
		        return OpcionalCupon.get();
	        }
			return null;
	    }//fin del metodo
		
		/**
		 * Metodo que añade a la base de datos un nuevo cupon
		 * @param cupon es el objeto cupon que se desea añadir, en caso de contar con el mismo id, actualiza los valores solamente
		 * @return el objeto cupon que fue almacenado
		 */
		@RequestMapping(value="crearCupon", method=RequestMethod.POST)
		public ResponseEntity<Cupon> crearCupon(@RequestBody Cupon miCupon)
		{
			Cupon nuevocupon = cuponRepository.save(miCupon);
			nuevocupon.setFechaModificacion(new Date());
			nuevocupon.setFechaCreacion(new Date());
			nuevocupon.setActivo(true);
			return ResponseEntity.ok(nuevocupon);
		}
		
		/**
		 * Metodo que modifica un cupon ya existente en la base de datos (el cupon debe existir sino sera creado uno nuevo)
		 * @param cupon es el objecto cupon que se quiere modificar
		 * @return objeto cupon ya modificado
		 */
		@RequestMapping(value="actualizarCupon", method=RequestMethod.PUT)
		public ResponseEntity<Cupon> actualizarCupon(@RequestBody Cupon miCupon)
		{
			Optional<Cupon> OpcionalCupon = cuponRepository.findById((UUID) miCupon.getId());
			if(OpcionalCupon.isPresent())
			{
				Cupon actualizarCupon = OpcionalCupon.get();
				actualizarCupon.setNombre(miCupon.getNombre());
				actualizarCupon.setDescripcion(miCupon.getDescripcion());
				actualizarCupon.setUsoMaximo(miCupon.getUsoMaximo());
				actualizarCupon.setCodigo(miCupon.getCodigo());
				actualizarCupon.setInicio(miCupon.getInicio());
				actualizarCupon.setFin(miCupon.getFin());
				actualizarCupon.setPaseExclusivo(miCupon.isPaseExclusivo());
				actualizarCupon.setTipo(miCupon.getTipo());
				actualizarCupon.setIdServ(miCupon.getIdServ());
				actualizarCupon.setActivo(miCupon.isActivo());
				actualizarCupon.setFechaModificacion(new Date());
				cuponRepository.save(actualizarCupon);
				return ResponseEntity.ok(actualizarCupon);
			}
			else
			{
				return ResponseEntity.notFound().build();
			}
		}
		
		@RequestMapping(value="eliminarCupon", method=RequestMethod.DELETE)
		public ResponseEntity<Void> eliminarCupon(@PathVariable("cuponId") UUID Id)
		{
			cuponRepository.deleteById(null);
			return ResponseEntity.ok(null);
		}
		
		//----------------------------------------CUPÓN APLICABLE-----------------------------------------------
		/**
		 * Metodo que muestra todos los CuponesAplicados almacenados en la base de datos
		 * @return lista de CuponesAplicados
		 */
		@RequestMapping(value="obtenerCuponAplicable", method= RequestMethod.GET)
		public ResponseEntity<List<CuponAplicado>> obtenerCuponAplicado()
		{
			List<CuponAplicado> cuponesAplicados = cuponAplicadoRepository.findAll();
			return ResponseEntity.ok(cuponesAplicados);
		}
		
		/**
		 * Metodo que muestra solo un cuponAplicado
		 * @param Id es el id del cuponAplicado que se quiere mostrar, en caso de no encontrarlo genera un RuntimeException
		 * @return El cuponAplicado con el id Id
		 */
		@GetMapping("/obtenerCuponAplicable/{id}")
		@ResponseBody
	    public CuponAplicado getCuponAplicado(@PathVariable UUID id){
			Optional<CuponAplicado> OpcionalCuponAplicado = cuponAplicadoRepository.findById(id);
			if(OpcionalCuponAplicado.isPresent())
			{
		        return OpcionalCuponAplicado.get();
	        }
			return null;
	    }//fin del metodo
		
		/**
		 * Metodo que añade a la base de datos un nuevo cuponAplicado
		 * @param cuponAplicado es el objeto cuponAplicado que se desea añadir, en caso de contar con el mismo id, actualiza los valores solamente
		 * @return el objeto cuponAplicado que fue almacenado
		 */
		@RequestMapping(value="crearCuponAplicado", method=RequestMethod.POST)
		public ResponseEntity<CuponAplicado> crearCuponAplicado(@RequestBody CuponAplicado miCuponAplicado)
		{
			CuponAplicado nuevoCuponAplicado = cuponAplicadoRepository.save(miCuponAplicado);
			nuevoCuponAplicado.setActivo(true);
			nuevoCuponAplicado.setFechaCreacion(new Date());
			nuevoCuponAplicado.setFechaModificiacion(new Date());
			return ResponseEntity.ok(nuevoCuponAplicado);
		}
		
		/**
		 * Metodo que modifica un cuponAplicado ya existente en la base de datos (el cuponAplicado debe existir sino sera creado uno nuevo)
		 * @param cuponAplicado es el objecto cuponAplicado que se quiere modificar
		 * @return objeto cuponAplicado ya modificado
		 */
		@RequestMapping(value="actulizarCuponAplicado", method=RequestMethod.PUT)
		public ResponseEntity<CuponAplicado> actualizarCuponAplicado(@RequestBody CuponAplicado miCuponAplicado)
		{
			Optional<CuponAplicado> OpcionalCuponAplicado = cuponAplicadoRepository.findById((UUID) miCuponAplicado.getId());
			if(OpcionalCuponAplicado.isPresent())
			{
				
				CuponAplicado actualizarCuponAplicado = OpcionalCuponAplicado.get();
				actualizarCuponAplicado.setIdMiembro(miCuponAplicado.getIdMiembro());
				actualizarCuponAplicado.setIdCupon(miCuponAplicado.getIdCupon());
				actualizarCuponAplicado.setFecha(miCuponAplicado.getFecha());
				actualizarCuponAplicado.setTipo(miCuponAplicado.getTipo());
				actualizarCuponAplicado.setCantidad(miCuponAplicado.getCantidad());
				actualizarCuponAplicado.setCodigoqr(miCuponAplicado.getCodigoqr());
				actualizarCuponAplicado.setIdDispositivo(miCuponAplicado.getIdDispositivo());
				actualizarCuponAplicado.setIdVentaDetalle(miCuponAplicado.getIdVentaDetalle());
				actualizarCuponAplicado.setActivo(miCuponAplicado.isActivo());
				actualizarCuponAplicado.setFechaModificiacion(new Date());
				return ResponseEntity.ok(actualizarCuponAplicado);
			}
			else
			{
				return ResponseEntity.notFound().build();
			}
		}
		
		@RequestMapping(value="eliminarCuponAplicado", method=RequestMethod.DELETE)
		public ResponseEntity<Void> eliminarcuponaplicado(@PathVariable("cuponAplicado") UUID id)
		{
			cuponAplicadoRepository.deleteById(null);
			return ResponseEntity.ok(null);
		}
}
