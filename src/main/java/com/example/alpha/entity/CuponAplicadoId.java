/*
 * Objetivo: Crear la clase CuponAplicado del proyecto para así relacionar nuestra tabla CuponAplicado,
 * quedando de la siguiente manera:
 * @autor: Daniel García Velasco Abimael Rueda Galindo
 * @version: 19/08/2021
 * */
package com.example.alpha.entity;

//Librerías
import java.io.Serializable;
import java.util.UUID;

/*
 *La clase da igualdad de valores para estos métodos debe ser coherente con la igualdad 
 * de la base de datos para los tipos de bases de datos a los que se asigna la clave.
 * */

public class CuponAplicadoId implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L; //Es un identificador de versión universal para una clase serializable.
	
	//Variables de la cual se le hará la composición
	private UUID id;
	private int IdMiembro;
	private int IdCupon;
	
	//Creación de Getters y Setters
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public int getIdMiembro() {
		return IdMiembro;
	}
	public void setIdMiembro(int idMiembro) {
		IdMiembro = idMiembro;
	}
	public int getIdCupon() {
		return IdCupon;
	}
	public void setIdCupon(int idCupon) {
		IdCupon = idCupon;
	}
	
	//Método hashCode 
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + IdCupon;
		result = prime * result + IdMiembro;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	//Método equals
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CuponAplicadoId other = (CuponAplicadoId) obj;
		if (IdCupon != other.IdCupon)
			return false;
		if (IdMiembro != other.IdMiembro)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "CuponAplicadoId [id=" + id + ", IdMiembro=" + IdMiembro + ", IdCupon=" + IdCupon + "]";
	}
	
	//Constructor
	public CuponAplicadoId(UUID id, int idMiembro, int idCupon) {
		this.id = id;
		IdMiembro = idMiembro;
		IdCupon = idCupon;
	}
	
	//Constructor
	public CuponAplicadoId() {
	}
	
	
}
