/*
 * Objetivo: Crear la clase CuponAplicado del proyecto para así relacionar nuestra tabla CuponAplicado,
 * quedando de la siguiente manera:
 * @autor: Daniel García Velasco Abimael Rueda Galindo
 * @version: 19/08/2021
 * */
package com.example.alpha.entity;

//Librerías
import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;


@Entity //Sirve únicamente para indicarle a JPA que esa clase es una Entity.
@Table(name="cuponaplicado") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
@IdClass(CuponAplicadoId.class)
public class CuponAplicado 
{
	@Id //Define la llave primaria.
    @GeneratedValue(generator = "UUID") //Se estableció un tipo de variable UUID 
    @GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false) //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private UUID id;
	
	@ManyToOne(fetch = FetchType.LAZY) //Relación mucho a uno con la tabla Miembro de la columna de la FK
	@MapsId("id")
	private Miembro IdMiembro;
	
	@ManyToOne(fetch = FetchType.LAZY) //Relación mucho a uno con la tabla Cupón de la columna de la FK
	@MapsId("id")
	private Cupon IdCupon;
	
	@Column(name = "fecha") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	@Temporal(TemporalType.TIMESTAMP)
	private Date Fecha;
	
	@OneToOne(fetch = FetchType.LAZY) //Relación uno a uno con la tabla CuponAplicado de la columna de la FK
    @MapsId("id")
	private CuponAplicado IdCuponAplicado;
	
	@Column(name="tipo") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private String Tipo;
	
	@Column(name="cantidad") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private int Cantidad;
	
	@Column(name="codigoqr") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private String Codigoqr;
	
	@OneToOne(fetch = FetchType.LAZY) //Relación uno a uno con la tabla Dispositivo de la columna de la FK
    @MapsId("id")
	private Dispositivo IdDispositivo;
	
	@Column(name="idventadetalle") //Se utliza pra marcar una propiedad
	private int IdVentaDetalle;

	@Column(name="activo") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private boolean Activo = true;
	
	@Column(name="fechaCreacion") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private Date fechaCreacion = new Date();
	
	@Column(name="fechaModificacion") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private Date FechaModificiacion = new Date();
	
	//Creación de getters y setters
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Miembro getIdMiembro() {
		return IdMiembro;
	}

	public void setIdMiembro(Miembro idMiembro) {
		IdMiembro = idMiembro;
	}

	public Cupon getIdCupon() {
		return IdCupon;
	}

	public void setIdCupon(Cupon idCupon) {
		IdCupon = idCupon;
	}

	public Date getFecha() {
		return Fecha;
	}

	public void setFecha(Date fecha) {
		Fecha = fecha;
	}

	public CuponAplicado getIdCuponAplicado() {
		return IdCuponAplicado;
	}

	public void setIdCuponAplicado(CuponAplicado idCuponAplicado) {
		IdCuponAplicado = idCuponAplicado;
	}

	public String getTipo() {
		return Tipo;
	}

	public void setTipo(String tipo) {
		Tipo = tipo;
	}

	public int getCantidad() {
		return Cantidad;
	}

	public void setCantidad(int cantidad) {
		Cantidad = cantidad;
	}

	public String getCodigoqr() {
		return Codigoqr;
	}

	public void setCodigoqr(String codigoqr) {
		Codigoqr = codigoqr;
	}

	public Dispositivo getIdDispositivo() {
		return IdDispositivo;
	}

	public void setIdDispositivo(Dispositivo idDispositivo) {
		IdDispositivo = idDispositivo;
	}

	public int getIdVentaDetalle() {
		return IdVentaDetalle;
	}

	public void setIdVentaDetalle(int idVentaDetalle) {
		IdVentaDetalle = idVentaDetalle;
	}

	public boolean isActivo() {
		return Activo;
	}

	public void setActivo(boolean activo) {
		Activo = activo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Date getFechaModificiacion() {
		return FechaModificiacion;
	}

	public void setFechaModificiacion(Date fechaModificiacion) {
		FechaModificiacion = fechaModificiacion;
	}

	@Override
	public String toString() {
		return "CuponAplicado [id=" + id + ", IdMiembro=" + IdMiembro + ", IdCupon=" + IdCupon + ", Fecha=" + Fecha
				+ ", IdCuponAplicado=" + IdCuponAplicado + ", Tipo=" + Tipo + ", Cantidad=" + Cantidad + ", Codigoqr="
				+ Codigoqr + ", IdDispositivo=" + IdDispositivo + ", IdVentaDetalle=" + IdVentaDetalle + "]";
	}

	//Constructor generado
	public CuponAplicado() {
	} 
	
	
}
