/*
 * Objetivo: Crear la clase Miembro del proyecto para así relacionar nuestra tabla Miembro,
 * quedando de la siguiente manera:
 * @autor: Daniel García Velasco Abimael Rueda Galindo
 * @version: 19/08/2021
 * */
package com.example.alpha.entity;

//Librerías
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;


@Entity //Sirve únicamente para indicarle a JPA que esa clase es una Entity.
@Table(name="miembro") //Se utiliza para poner el nombre real de la tabla en la base de datos
public class Miembro 
{

	@Id //Define la llave primaria.
    @GeneratedValue(generator = "UUID") //Se estableció un tipo de variable UUID 
    @GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false) //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
    private UUID id;
	
	@Column(name="nombre") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private String Nombre;
	
	@Column(name="club") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private String Club;
	
	@Column(name="tipomembresia") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private String TipoMembresia;

	@Column(name="activo") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private boolean Activo=true;
	
	@Column(name = "fechacreacion") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	@Temporal(TemporalType.TIMESTAMP)
	private Date FechaCreacion=new Date();
	
	@Column(name = "fechamodificacion") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	@Temporal(TemporalType.TIMESTAMP)
	private Date FechaModificacion=new Date();
	
	//Creación de los getters y setters
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getClub() {
		return Club;
	}

	public void setClub(String club) {
		Club = club;
	}

	public String getTipoMembresia() {
		return TipoMembresia;
	}

	public void setTipoMembresia(String tipoMembresia) {
		TipoMembresia = tipoMembresia;
	}

	public boolean isActivo() {
		return Activo;
	}

	public void setActivo(boolean activo) {
		Activo = activo;
	}

	public Date getFechaCreacion() {
		return FechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return FechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		FechaModificacion = fechaModificacion;
	}

	@Override
	public String toString() {
		return "Miembro [id=" + id + ", Nombre=" + Nombre + ", Club=" + Club + ", TipoMembresia=" + TipoMembresia
				+ ", Activo=" + Activo + ", FechaCreacion=" + FechaCreacion + ", FechaModificacion=" + FechaModificacion
				+ "]";
	}
	
}
