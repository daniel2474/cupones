/*
 * Objetivo: Crear la clase Cupón del proyecto para así relacionar nuestra tabla Cupón,
 * quedando de la siguiente manera:
 * @autor: Daniel García Velasco Abimael Rueda Galindo
 * @version: 19/08/2021
 * */
package com.example.alpha.entity;

//Librerías
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity //Sirve únicamente para indicarle a JPA que esa clase es una Entity.
@Table(name="cupon") //Se utiliza para poner el nombre real de la tabla en la base de datos
public class Cupon 
{
	@Id //Define la llave primaria.
    @GeneratedValue(generator = "UUID") //Se estableció un tipo de variable UUID 
    @GenericGenerator(
        name = "UUID",
        strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false) //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
    private UUID id;
	
	@Column(name="nombre") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private String Nombre;
	
	@Column(name="descripcion") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private String Descripcion;
	
	@Column(name="uso_maximo") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private int UsoMaximo;
	
	@Column(name="codigo") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private String Codigo;
	
	@Column(name = "inicio") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	@Temporal(TemporalType.DATE)
	private Date Inicio;
	
	@Column(name = "fin") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	@Temporal(TemporalType.DATE)
	private Date fin;
	
	@Column(name="pase_exclusivo") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	boolean PaseExclusivo;
	
	@Column(name="tipo") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	String Tipo;
	
	@Column(name="id_serv") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	int IdServ;

	@Column(name="activo") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	private boolean Activo = true;
	
	@Column(name = "fechacreacion") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	@Temporal(TemporalType.TIMESTAMP)
	private Date FechaCreacion = new Date();
	
	@Column(name = "fechamodificacion") //Permite establecer el nombre de la columna de la tabla con la que el atributo debe de mapear.
	@Temporal(TemporalType.TIMESTAMP)
	private Date FechaModificacion = new Date();
	
	//Creación de getters y setters
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getDescripcion() {
		return Descripcion;
	}

	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}

	public int getUsoMaximo() {
		return UsoMaximo;
	}

	public void setUsoMaximo(int usoMaximo) {
		UsoMaximo = usoMaximo;
	}

	public String getCodigo() {
		return Codigo;
	}

	public void setCodigo(String codigo) {
		Codigo = codigo;
	}

	public Date getInicio() {
		return Inicio;
	}

	public void setInicio(Date inicio) {
		Inicio = inicio;
	}

	public Date getFin() {
		return fin;
	}

	public void setFin(Date fin) {
		this.fin = fin;
	}

	public boolean isPaseExclusivo() {
		return PaseExclusivo;
	}

	public void setPaseExclusivo(boolean paseExclusivo) {
		PaseExclusivo = paseExclusivo;
	}

	public String getTipo() {
		return Tipo;
	}

	public void setTipo(String tipo) {
		Tipo = tipo;
	}

	public int getIdServ() {
		return IdServ;
	}

	public void setIdServ(int idServ) {
		IdServ = idServ;
	}
	
	public boolean isActivo() {
		return Activo;
	}

	public void setActivo(boolean activo) {
		Activo = activo;
	}

	public Date getFechaCreacion() {
		return FechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}

	public Date getFechaModificacion() {
		return FechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		FechaModificacion = fechaModificacion;
	}

}
