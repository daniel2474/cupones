## Proyecto
Alpha - Web Service (Cupones) 
## Descripción
El programa que se está implementado para el ALPHA es una API desde el programa SpringTool y se crean un nuevo proyecto con las diferentes clases que lo componen, los dao's y un controlador para así poder crear métodos HTTP. También tenemos instalado SQL Server para nuestra base de datos. 
El postman simplmente lo ocupamos para hacer peticiones de los diferentes métodos que creamos en la API.
Esta web service está basado en los cupones de los clientes en el cual está compuesto por MIEMBRO, DISPOSITIVO, CUPON y CUPONESAPLICADOS.
## Instalación
Para la creación de este proyecto se intalarón los siguientes programas, todos los programas está para windows:
[SpringTool] https://spring.io/tools/
[SQL Server] https://www.microsoft.com/es-mx/sql-server/sql-server-downloads
[Postman] https://www.postman.com/downloads/
## Uso
En el springtool debes de buscar el programa de la API, se debe de dar clic derecho al archivo ir a Run As y luego Spring Boot App y comenzará a ejecutarse y podrás hacer peticiones desde el postman para agregar, mostrar o modificar información desde la base de datos. Al ejecutar el programa no es necesario tener la base de datos por que se crea en autómatico a la hora de compilar la web service, recuerda que deberás meter el usuario y contraseña y en el nombre de tu servidor del SQL Server para así conectarse a la base de datos o de lo contrario te enviará error la web service. 
## Autores
DANIEL GARCÍA VELASCO
ABIMAEL RUEDA GALINDO